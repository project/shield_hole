<?php

namespace Drupal\shield_hole;

use Drupal\shield\ShieldMiddleware;
use Symfony\Component\HttpFoundation\Request;

/**
 * ShieldOverride decorates the ShieldMiddleware to bypass according to config.
 *
 * @package Drupal\shield_hole
 */
class ShieldOverride extends ShieldMiddleware {

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
    // Get the raw current path.
    $currentPath = $request->getPathInfo();

    // Get the current method (e.g. GET or POST).
    $currentMethod = $request->getMethod();

    // Fetch my config.
    $config = \Drupal::config('shield_hole.settings');
    $urls = array_filter($config->get('urls'));

    // If method is POST or GET and path is in the $urls array.
    if (($currentMethod === 'POST' || $currentMethod === 'GET')
    && (in_array($currentPath, $urls) || $this->isValidWildcardUrl($currentPath, $urls))) {
      // If we are attempting to access the service then we handle the
      // request without invoking the Shield module.
      return $this->httpKernel->handle($request, $type, $catch);
    }

    // Always handle the request using the default Shield behaviour.
    return parent::handle($request, $type, $catch);
  }

  /**
   * Check if the partial section current path exists in the configured list.
   *
   * @param string $currentPath
   *   Current requested path.
   * @param string $urls
   *   Urls list from config.
   *
   * @return bool
   *   Returns boolean value depending upon check.
   */
  private function isValidWildcardUrl($currentPath, $urls) {
    if (is_array($urls) && count($urls) > 0 && $currentPath != '') {
      foreach ($urls as $url) {
        if (substr($url, -1) == '*') {
          $url = str_replace('/', "\\/", rtrim($url, "*"));
          if (count(preg_grep('/^' . $url . '/', [$currentPath]))) {
            return TRUE;
          }
        }
      }
    }
    return FALSE;
  }

}
